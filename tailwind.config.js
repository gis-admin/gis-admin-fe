module.exports = {
  content: [
    "./src/**/*.{js,jsx}",
  ],
  theme: {
    fontFamily: {
      sans: ['"Noto Sans TC"'],
      mono: ['"Eras Bold"'],
    },
    extend: {
      colors: {
        blue: {
          950: '#1B4980',
        },
        orange: {
          950: '#EE5A0A'
        },
      },
      dropShadow: {
        '3xl': '0 10px 10px rgba(0, 0, 0, 1)',
      },
      boxShadow: {
        '3xl': '0 10px 10px -3px rgba(0, 0, 0, 1)',
      },
      minWidth: {
        '1/2': '50%',
        '1/3': '33.33%',
        '1/6': '16.67%',
      },
    },
  },
  plugins: [],
}
